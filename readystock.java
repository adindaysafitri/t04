class readystock extends kue{

    private double jumlah;

    public readystock(String name, double price, double jumlah) {
        super(name, price);
        this.jumlah = jumlah;
    }

    //menghitung harga kue jadi menggunakan variabel superclass "getPrice"
    public double hitungHarga(){
        return super.getPrice()*jumlah*2;
    }

    //memanggil method superclass
    @Override 
    public double Jumlah(){
        return jumlah;
    }
}