class pesanan extends kue {

    private int berat;

    public pesanan(String name, double price, int berat) {
        super(name, price);
        this.berat = berat;
    }
    
    //menghitung harga kue pesanan menggunakan variabel superclass "getPrice"
    public double hitungHarga(){
        return super.getPrice()*berat;
    }

    @Override
    public double Berat(){
        return berat;
    }
    
}